   
 * Introduction
 Automated copyright provides a block with an automated copyright statement. 
i.e. (c) 2001-2016 drupal.org All Rights Reserved 
The end year(currect year) will change automatically to the current year. 
No need to change this each year!
Given a start year, the currect year will automatically append to it with 
a dash(-) in between.When the start year and the current year are the same, 
only the current year will be displayed

 * Requirements
 There are no requirements to install this module
 
 * Installation
 Install just like any other module
 
 * Configuration
 Place the Automated_copyright block in Block Layout, and configure it as 
 you place it.
  -Owner = the owner of the intellectual property. This can be a person, 
   group or domain etc.
  -Start year = The year the intellectual property was made. If you enter 2001,
   the module will automatically conver it to 2001-2016 (2016=current year).
 When the start year is the same as the current year, it will only display the current year.

 * Maintainers
 https://www.drupal.org/u/thibaultf 
 Feel free to contact me.
