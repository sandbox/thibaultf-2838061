<?php

namespace Drupal\automated_copyright\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides AutomatedCopyrightBlock.
 *
 * @Block(
 *   id = "automated_copyright",
 *   admin_label = @Translation("Copyright")
 *   category = @Translation("Blocks")
 * )
 */
class AutomatedCopyrightBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'start_year' => date('Y'),
      'owner' => $this->configuration['owner'],
      'uppercase' => $this->configuration['uppercase'],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['owner'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Owner'),
      '#description' => $this->t('Fill in the owner of the intellectual property here'),
      '#default_value' => $this->configuration['owner'],
      '#required' => TRUE,
    );
    $form['start_year'] = array(
      '#title' => $this->t('Start year'),
      '#type' => 'number',
      '#min' => '1970',
      '#max' => date('Y'),
      '#default_value' => $this->configuration['start_year'],
      '#description' => $this->t('Select the year when the intellectual Property was made'),
    );
    $form['uppercase'] = array(
      '#title' => $this->t('All in uppercase?'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['uppercase'],
      '#description' => $this->t('Leave this unchecked to display the original entered format.'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['owner'] = $form_state->getValue('owner');
    $this->configuration['start_year'] = $form_state->getValue('start_year');
    $this->configuration['uppercase'] = $form_state->getValue('uppercase');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $start_year = $this->configuration['start_year'];
    $current_year = date('Y');
    $start_year = !empty($start_year) ? $start_year : $current_year;
    $yeartext = $start_year < $current_year ? $start_year . '-' . $current_year : $start_year;
    $owner = $this->configuration['owner'];
    $fulltext = '&copy; ' . $yeartext . '&nbsp;' . $owner . '&nbsp;' . $this->t('All Rights Reserved.');
    return array(
      '#type' => 'markup',
      '#markup' => $this->configuration['uppercase'] ? strtoupper($fulltext) : $fulltext,
    );
  }

}
