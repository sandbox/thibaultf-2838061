<?php

namespace Drupal\automated_copyright\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * @file
 * Controller routines for routes.
 */
class AutomatedCopyrightController extends ControllerBase {

  /**
   * Explanation what the Automated copyright block is about.
   */
  public function description() {
    // Make a link from a route to the block admin page.
    $block_admin_link = Link::createFromRoute($this->t('the block admin page'), 'block.admin_display')->toString();

    // Put the link into the content.
    $build = array(
      '#markup' => $this->t('The Automated copyright block provides a block with a copyright statement. You can enter a start year and it will automatically extend it with the current year if necessary. Enable and configure them on @block_admin_link', ['@block_admin_link' => $block_admin_link]),
    );

    return $build;
  }

}
